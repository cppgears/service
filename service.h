#pragma once
#include <atomic>
#include <csignal>
#include "traits/commandline.h"
#include "traits/debug.h"
#include "traits/filesystem.h"
#include "traits/sheduler.h"
#include "traits/signal.h"

namespace snippet {
	class service {
	protected:
		/* Run before main. Initalize service, parse commandline option
		*/
		virtual bool initialize(int argc, const char* argv[], const char* working_directory, const char* programm_name);
		virtual bool main();
		virtual void finalize();

		service();
		virtual ~service();
	protected:
		trait::service::scheduler	scheduler;
		trait::service::signal		signal;
		trait::service::debug		debug;
	protected:
		void exit(int code);
		void loop(size_t millisecond = 500, std::function<bool()> idle = {});
	private:
		std::atomic_bool	service_yet;
		int					service_exit_code;
	public:
		int run(int argc, const char* argv[]);
		
	};
}