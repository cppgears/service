#pragma once
#include <functional>
#include <csignal>
#include <unordered_map>
#include <vector>

namespace snippet {
	class service;
	namespace trait {
		namespace service {
			class signal {
			protected:
				friend class snippets::service;
				signal() { ; }
				~signal() { restore(); }
			public:
				template<typename ... SGNLS>
				void handle(std::function<void(sig_atomic_t)> fn, SGNLS ... sgnls) {
					static std::unordered_map<int, signal* > __signals;
					_signals = &__signals;
					_handler = fn;
					for (auto& sig : std::vector<int>({ sgnls... })) {
						if (sig >= 0 && sig < _NSIG) {
							__signals.emplace(sig, this);
							_prevhandlers.emplace_back(sig, ::signal(sig, [](int sig) {
								auto it = __signals.find(sig);
								if (it != __signals.end()) {
									it->second->_handler(sig);
								}
							}));
						}
					}
				}
				inline void restore() {
					for (auto&& s : _prevhandlers) {
						_signals->erase(s.first);
						::signal(s.first, s.second);
					}
					_prevhandlers.clear();
				}
			private:
				std::function<void(sig_atomic_t)>		_handler;
				std::unordered_map<int, signal*>*		_signals;
				std::vector<std::pair<int, __sighandler_t>> _prevhandlers;
			};
		}
	}
}