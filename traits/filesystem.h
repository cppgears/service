#pragma once

namespace snippet {
	class service;
	namespace trait {
		namespace service {
			class filesystem {
			protected:
				friend class snippets::service;
				filesystem() { ; }
			public:
			};
		}
	}
}