#pragma once
#include <cstdio>
#include <execinfo.h>
#include <memory>

namespace snippet {
	class service;
	namespace trait {
		namespace service {
			class debug {
			protected:
				friend class snippets::service;
				debug() { ; }
			public:
				inline void backtrace(const char* space) {
					void *buffer[100];
					char **strings;
					int nptrs = ::backtrace(buffer, 100);
					fprintf(stderr, "[ %s ] backtrace() returned %d addresses\n", space, nptrs);
					strings = ::backtrace_symbols(buffer, nptrs);
					if (strings == nullptr) { fprintf(stderr, "[ %s ] backtrace_symbols() no backtrace symbols\n", space); }
					else {
						for (int j = 0; j < nptrs; j++)	fprintf(stderr, "%s\n", strings[j]);
						free(strings);
					}
				}
			};
		}
	}
}