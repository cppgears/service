#pragma once
#include <list>
/* Delay execution. Load list, update config, etc... */
namespace snippet {
	class service;
	namespace trait {
		namespace service {
			class scheduler {
			protected:
				friend class snippets::service;
				scheduler() { ; }
				~scheduler() {
					for (auto&& task : tasks) {
						delete std::get<2>(task);
					}; 
				}
				inline void check_run() {
					for (auto&& task : tasks) {
						if (time(nullptr) >= std::get<0>(task)) {
							std::get<0>(task) = time(nullptr) + std::get<1>(task);
							if (!(*(std::get<2>(task)))()) {
								delete std::get<2>(task);
								tasks.pop_back();
							}
						}
					}
				}
			public:
				class job {
				public:
					virtual bool operator()() = 0;
					virtual ~job() { ; }
				};
				template<typename JOB>
				scheduler& task(JOB* task, size_t period_seconds, bool execute_now = false) {
					tasks.emplace_back(0, period_seconds, (job*)task);
					if (execute_now) {
						std::get<0>(tasks.back()) = time(nullptr) + period_seconds;
						if (!(*(std::get<2>(tasks.back())))()) {
							delete std::get<2>(tasks.back());
							tasks.pop_back();
						}
					}
					return *this;
				}
			private:
				std::list<std::tuple<time_t /*last execution time*/, size_t /* period seconds */, job*>> tasks;
			};
		}
	}
}