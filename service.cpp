#include "service.h"
#include <string>
#include <cstdio>
#include <thread>
#include <functional>


using namespace snippet;

service::service() : service_yet(false), service_exit_code(0) {
}

service::~service() {
}

void service::exit(int code) {
	printf("exit with code: %d\n", code);
	service_yet = false;
	service_exit_code = code;
}

void service::loop(size_t millisecond, std::function<bool()> idle) {
	while (service_yet)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(millisecond ? millisecond : 500));

		scheduler.check_run();

		if (idle) {
			idle();
		}
	}
}

bool service::initialize(int argc, const char* argv[], const char* working_directory, const char* programm_name) {
	return false;
}

bool service::main() {
	return false;
}

void service::finalize() {
}

int service::run(int argc, const char* argv[]) {
	
	if (initialize(argc, argv, nullptr, nullptr)) {
		
		try {
			service_yet = true;
			main();
			service_yet = false;
		}
		catch (...) {
			service_yet = false;
			debug.backtrace(__PRETTY_FUNCTION__);
		}

		finalize();
	}
	return service_exit_code;
}
